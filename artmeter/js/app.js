var calcApp = angular.module('calcApp', ['firebase']);

var config = {
    apiKey: "AIzaSyAmEJ4KVTT-n-s_wItj5lJwmO4_uzebV1g",
    authDomain: "shopy-calc.firebaseapp.com",
    databaseURL: "https://shopy-calc.firebaseio.com",
    projectId: "shopy-calc",
    storageBucket: "shopy-calc.appspot.com",
    messagingSenderId: "374612128203"
};

firebase.initializeApp(config);

calcApp.config(['$firebaseRefProvider', function ($firebaseRefProvider) {

    $firebaseRefProvider.registerUrl({
        default: config.databaseURL,
        materials: config.databaseURL + '/materials',
        dtos: config.databaseURL + '/dtos'
    });

}]);

calcApp.directive('onEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.onEnter);
                });
                event.preventDefault();
            }
        });
    };
});

calcApp.controller('calcController', ['$scope', '$http', '$timeout', "$firebaseObject", "$firebaseArray", "$firebaseAuth", "$firebaseRef", function ($scope, $http, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseRef) {

    $scope.price = '0';
    $scope.result = '0';
    $scope.iva = '0';

    $scope.materials = $firebaseArray($firebaseRef.materials);
    $scope.dtos = $firebaseArray($firebaseRef.dtos);

    $scope.calcPrice = function () {

        if (($scope.xVal / 100) * ($scope.yVal / 100) < 0.26) {
            $scope.price = (($scope.xVal / 100) * ($scope.yVal / 100) * $scope.materialSelected) * 1.5;
            $scope.price = ((($scope.xVal / 100) * ($scope.yVal / 100) * $scope.materialSelected) * 1.5);
        } else if (($scope.xVal / 100) * ($scope.yVal / 100) < 0.52) {
            $scope.price = (($scope.xVal / 100) * ($scope.yVal / 100) * $scope.materialSelected) * 1.20;
        } else if (($scope.xVal / 100) * ($scope.yVal / 100) < 0.78) {
            $scope.price = (($scope.xVal / 100) * ($scope.yVal / 100) * $scope.materialSelected) * 1.10;
        } else if (($scope.xVal / 100) * ($scope.yVal / 100) >= 1.82) {
            $scope.price = (($scope.xVal / 100) * ($scope.yVal / 100) * $scope.materialSelected) / 1.10;
        } else if (($scope.xVal / 100) * ($scope.yVal / 100) >= 3.64) {
            $scope.price = (($scope.xVal / 100) * ($scope.yVal / 100) * $scope.materialSelected) / 1.20;
        } else if (($scope.xVal / 100) * ($scope.yVal / 100) >= 5.46) {
            $scope.price = (($scope.xVal / 100) * ($scope.yVal / 100) * $scope.materialSelected) / 1.30;
        } else {
            $scope.price = ($scope.xVal / 100) * ($scope.yVal / 100) * $scope.materialSelected;
        }

        $scope.result = eval($scope.price);
        $scope.iva = eval($scope.price) * 1.21;

        $scope.minPrice = function () {
            if ($scope.result < 4) {
                $scope.result = 4;
                $scope.iva = $scope.result * 1.21;
            }
        };

        $scope.minPrice();

    };

    $scope.resetPrice = function () {
        $scope.price = '0';
        $scope.result = '0';
        $scope.iva = '0';
        $scope.xVal = '';
        $scope.yVal = '';
        $scope.materialSelected = '';
        $scope.dtoSelected = '';
    };

    $scope.addDto = function () {

        $scope.calcPrice();

        if ($scope.dtoSelected === '' || $scope.dtoSelected === '0') {
            $scope.iva = $scope.result * 1.21;
        } else {
            $scope.result = ($scope.result / $scope.dtoSelected);
            $scope.iva = ($scope.result / $scope.dtoSelected) * 1.21;
        }

    };

    $scope.resetDto = function () {
        $scope.dtoSelected = ''
    };

    //functions
    $scope.goOpts = function () {
        $('.modal').addClass('show-modal');
        $('#calcMeter').css('opacity', '0');
    };

    $scope.goClose = function () {
        $('.modal').removeClass('show-modal');
        $('#calcMeter').css('opacity', '1');
    };

    //new inputs clean
    $scope.cleanInputs = function(){
        $('.row input').val('');
    };

}]);