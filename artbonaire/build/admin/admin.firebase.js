app.adminFirebase = function ($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {
  var auth = $firebaseAuth();

  // usuario conectado ----------------------------------------------------

  auth.$onAuthStateChanged(function (firebaseUser) {

    if (firebaseUser) {
      console.log('User connection')
    } else {
      console.log('No user connection')
    }
  });

}