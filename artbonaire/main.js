//app name
var app = angular.module('app', ['ngRoute', 'ngAnimate', 'firebase']);

//firebase auth
const config = {
    apiKey: "AIzaSyDrqBlmo2Wug_f-_qE7l2IM9ps5fXWeAnM",
    authDomain: "artbonaire-web.firebaseapp.com",
    databaseURL: "https://artbonaire-web.firebaseio.com",
    projectId: "artbonaire-web",
    storageBucket: "artbonaire-web.appspot.com",
    messagingSenderId: "373957482747"
};

firebase.initializeApp(config);

app.factory("Auth", ["$firebaseAuth", function ($firebaseAuth) {
    return $firebaseAuth();
}]);

// !auth redirections
app.run(["$rootScope", "$location", function ($rootScope, $location) {
    $rootScope.$on("$routeChangeError", function (event, next, previous, error) {
        if ($error === "AUTH_REQUIRED") {
            $location.path("/login");
        }
    });
}]);


//on-enter=""
app.directive('onEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.onEnter);
                });

                event.preventDefault();
            }
        });
    };
});

//$locations
app.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
        .when('/servicios', {
            templateUrl: './build/servicios/servicios.html',
            controller: 'appController',
        })
        .when('/trabajos', {
            templateUrl: './build/trabajos/trabajos.html',
            controller: 'appController',
        })
        .when('/catalogo', {
            templateUrl: './build/catalogo/catalogo.html',
            controller: 'appController',
        })
        .when('/nosotros', {
            templateUrl: './build/nosotros/nosotros.html',
            controller: 'appController',
        })
        .when('/contacto', {
            templateUrl: './build/contacto/contacto.html',
            controller: 'appController',
        })
        .when('/admin', {
            templateUrl: './build/admin/admin.html',
            controller: 'appController',
            resolve: {
                "currentAuth": ["Auth", function (Auth) {
                    return Auth.$requireSignIn();
                }]
            }
        })
        .otherwise({
            redirectTo: '/servicios',
        });

}]);

//js source
app.controller('appController', ['$scope', '$location', '$window', '$timeout', '$firebaseObject', '$firebaseArray', '$firebaseAuth', '$firebaseStorage', function ($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {

    //views
    app.servicios($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    app.trabajos($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    app.catalogo($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    app.nosotros($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    app.contacto($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    
    //firebase
    app.trabajosFirebase($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    app.admin($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    app.adminFirebase($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);


    //routes fucntions
    $scope.goLogin = function () {
        $location.path('/login')
    }

}]);