app.appFirebase = function ($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {
    var auth = $firebaseAuth();
    var db = firebase.firestore();

    $scope.signOut = function () {
        auth.$signOut();
        $location.path('/login');
    }

    // refs
    var ordenesRef = db.collection('ordenes');

    //realtime db
    ordenesRef.onSnapshot(function (querySnapshot) {
        var ordenes = [];
        querySnapshot.forEach(function (doc) {
            ordenes.push(doc.data());
        });
        $timeout(function () {
            $scope.ordenes = ordenes;

            var ordenNums = [];
            ordenes.forEach(function (ord) {
                var toNum = parseInt(ord.num);
                ordenNums.push(toNum);
            })
            $timeout(function () {

                function filter_array(test_array) {
                    var index = -1,
                        arr_length = test_array ? test_array.length : 0,
                        resIndex = -1,
                        result = [];

                    while (++index < arr_length) {
                        var value = test_array[index];

                        if (value) {
                            result[++resIndex] = value;
                        }
                    }

                    return result;
                }

                function getMaxOfArray(numArray) {
                    return Math.max.apply(null, numArray);
                }

                $scope.nextNum = getMaxOfArray(filter_array(ordenNums)) + 1;
            })
        });
    });

    $scope.today = new Date();

    //create order
    $scope.newOrder = function () {

        if(this.num == '') {
            this.num = 'S/N'
        };

        ordenesRef.add({
            num: this.num,
            cliente: this.cliente,
            trabajador: this.trabajador,
            entrega: this.entrega,
            desc: this.desc,
            date: new Date()
        }).then(function (docRef) {
            ordenesRef.doc(docRef.id).update({
                id: docRef.id
            });
        });

        $scope.hideModal();

    };

    $scope.editOrder = function (orden) {
        $scope.modal = true;
        $scope.newBtn = false;
        $scope.editBtn = true;
        ordenesRef.doc(orden.id).get().then(function (doc) {
            $timeout(function () {
                $scope.edit = doc.data();
                $scope.num = $scope.edit.num;
                $scope.cliente = $scope.edit.cliente;
                $scope.trabajador = $scope.edit.trabajador || '';
                $scope.entrega = $scope.edit.entrega || '';
                $scope.desc = $scope.edit.desc || '';
            });
        })
    }

    $scope.changeOrder = function () {

        ordenesRef.doc($scope.edit.id).update({
            num: this.num,
            cliente: this.cliente,
            trabajador: this.trabajador,
            entrega: this.entrega,
            desc: this.desc,
            date: new Date()
        });
        $scope.hideModal();

    };

    //user state
    auth.$onAuthStateChanged(function (firebaseUser) {

        if (firebaseUser) {

            //remove order
            $scope.removeOrder = function (orden) {
                var sure = window.confirm('¿Seguro que quieres borrar?');
                if (sure) {
                    console.log(orden.id + ' borrada');
                    ordenesRef.doc(orden.id).delete();
                } else {
                    console.log(orden.id + ' no borrada');
                }
            }

            $scope.checkOrder = function (orden) {
                var sure = window.confirm('¿Seguro que quieres archivar?');
                if (sure) {
                    console.log(orden.id + ' archivada');
                    ordenesRef.doc(orden.id).update({
                        trabajador: 'archivado'
                    })
                } else {
                    console.log(orden.id + ' no archivada');
                }
            }

        }

    });

}