app.app = function ($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {

    $scope.modal = false;
    $scope.newBtn = false;
    $scope.editBtn = false;
    $scope.filterTask = '';

    $scope.showModal = function () {
        if ($scope.ordenes.length > 100) {
            var sure = window.prompt('¡CUIDADO! hay muchos trabajos en cola. El tiempo de entrega tendra una demora. Escribe "confirmar" para continuar', '');
            if (sure === 'confirmar') {
                $scope.modal = true;
                $scope.newBtn = true;
                $scope.num = $scope.nextNum;
                $scope.entrega = new Date();
            }
        } else {
            $scope.modal = true;
            $scope.newBtn = true;
            $scope.num = $scope.nextNum;
            $scope.entrega = new Date();
        }

        $timeout(function () {
            $('.first-input').focus()
        }, 500);
    };

    $scope.hideModal = function () {
        $scope.modal = false;
        $scope.newBtn = false;
        $scope.editBtn = false;
        $scope.edit = null;
        $scope.num = null;
        $scope.cliente = null;
        $scope.desc = null;
        $scope.trabajador = null;
        $scope.entrega = null;
    };

    var recognition;
    var recognizing = false;

    if (!('webkitSpeechRecognition' in window)) {
        console.log("¡Atención! El reconocimiento de voz no esta soportado por este navegador");
    } else {

        recognition = new webkitSpeechRecognition();
        recognition.lang = "es-Es";
        recognition.continuous = true;
        recognition.interimResults = true;

        recognition.onstart = function () {
            recognizing = true;
            console.log("Escuchando...");
        };

        recognition.onresult = function (event) {
            for (var i = event.resultIndex; i < event.results.length; i++) {
                if (event.results[i].isFinal)
                    $(".desc").val(event.results[i][0].transcript);
                $scope.desc = event.results[i][0].transcript;
            }
        };

        recognition.onerror = function (event) {}
        recognition.onend = function () {
            recognizing = false;
            $(".mic-icon").css('opacity', '1');
            console.log("terminó de escuchar, llegó a su fin");
        };
    };

    $scope.procesar = function () {
        if (recognizing == false) {
            $(".desc").val('');
            recognition.start();
            recognizing = true;
            $(".mic-icon").css('opacity', '.5');
        } else {
            recognition.stop();
            recognizing = false;
            $(".mic-icon").css('opacity', '1');
        }
    };
}