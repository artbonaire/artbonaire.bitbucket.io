app.login = function ($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {

  $scope.signForm = true;
  $scope.newForm = false;

  $scope.toggleForm = function () {
    $scope.signForm = $scope.signForm ? false : true;
    $scope.newForm = $scope.newForm ? false : true;
  }

}