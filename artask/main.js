//app name
var app = angular.module('app', ['ngRoute', 'ngAnimate', 'firebase']);

//firebase auth
const config = {
    apiKey: "AIzaSyDrqBlmo2Wug_f-_qE7l2IM9ps5fXWeAnM",
    authDomain: "artbonaire-web.firebaseapp.com",
    databaseURL: "https://artbonaire-web.firebaseio.com",
    projectId: "artbonaire-web",
    storageBucket: "artbonaire-web.appspot.com",
    messagingSenderId: "373957482747"
};

firebase.initializeApp(config);

app.factory("Auth", ["$firebaseAuth",
    function ($firebaseAuth) {
        return $firebaseAuth();
    }]);

// !auth redirections
app.run(["$rootScope", "$location",
    function ($rootScope, $location) {
        $rootScope.$on("$routeChangeError",
            function (event, next, previous, error) {
                if (error === "AUTH_REQUIRED") {
                    $location.path("/login");
                }
            });
    }]);


//on-enter
app.directive('onEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress",
            function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.onEnter);
                    });
                    event.preventDefault();
                }
            });
    };
});

//on-esc
app.directive('onEsc', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress",
            function (event) {
                if (event.which === 27) {
                    scope.$apply(function () {
                        scope.$eval(attrs.onEsc);
                    });
                    event.preventDefault();
                }
            });
    };
});

//on-alt
app.directive('onAlt', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress",
            function (event) {
                if (event.which === 18) {
                    scope.$apply(function () {
                        scope.$eval(attrs.onEsc);
                    });
                    event.preventDefault();
                }
            });
    };
});

//$locations
app.config(['$routeProvider',
    function ($routeProvider) {

        $routeProvider
            .when('/login', {
                templateUrl: './build/login/login.html',
                controller: 'appController',
            })
            .when('/app', {
                templateUrl: './build/app/app.html',
                controller: 'appController',
                // !auth ? auth
                resolve: {
                    "currentAuth": ["Auth",
                        function (Auth) {
                            return Auth.$requireSignIn();
                        }]
                }
            })
            .otherwise({
                //404?
                redirectTo: '/login',
            });

    }]);

//js source
app.controller('appController', ['$scope', '$location', '$window', '$timeout', '$firebaseObject', '$firebaseArray', '$firebaseAuth', '$firebaseStorage',
    function ($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {

        //login
        app.login($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
        app.loginFirebase($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);

        //app
        app.app($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
        app.appFirebase($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);

    }]);