app.nav = function ($scope, $rootScope, $location, $window, $timeout, $interval, $firebaseAuth, $firebaseStorage) {

    $scope.goCrm = function(){
        $location.path('/crm');
    }

    $scope.goCalc = function(){
        $location.path('/calc');
    }

    $scope.goUpload = function(){
        $location.path('/upload');
    }

}