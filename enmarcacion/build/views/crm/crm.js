app.crm = function ($scope, $http, $location, $window, $timeout, $interval) {

    $http.get('./assets/json/molduras_2018.json').then(function (response) {
        $scope.moldurasData = response.data;
        $scope.gridCrm.data = $scope.moldurasData;
    });

    $scope.gridCrm = {
        enableFiltering: true,
        columnDefs: [{
                field: 'P',
                displayName: 'PROVEEDOR',
                width: '*',
                minWidth: 170
            },
            {
                field: 'Nº de serie',
                displayName: 'REFERENCIA',
                width: '*',
                minWidth: 170
            },
            {
                field: 'Descripción de producto',
                displayName: 'DESCRIPCIÓN',
                width: '***',
                minWidth: 250
            },
            {
                field: 'Pérdidas',
                displayName: 'PÉRDIDAS',
                width: '*',
                minWidth: 170
            },
            {
                field: 'Precio',
                displayName: 'PRECIO',
                width: '*',
                minWidth: 170
            },

        ]
    }
}