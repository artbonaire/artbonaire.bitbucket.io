app.calc = function ($scope, $http, $location, $window, $timeout, $interval) {

    //precios base
    $scope.precioMoldura = 0;
    $scope.precioComplemento = 0;
    $scope.precioAcabado = 0;

    //precios finales
    $scope.precioProveedor = 0;
    $scope.pvp = 0;

    //precio m2
    $scope.widthSize = '';
    $scope.heightSize = '';

    $http.get('./assets/json/molduras_2018_final.json').then(function (response) {
        $scope.moldurasData = response.data;
        $scope.gridCalc.data = $scope.moldurasData;
    });

    $http.get('./assets/json/complementos.json').then(function (response) {
        $scope.complementos = response.data;
    });

    $http.get('./assets/json/acabados.json').then(function (response) {
        $scope.acabados = response.data;
    });

    $scope.gridCalc = {
        enableFiltering: true,
        multiSelect: false,
        columnDefs: [{
                field: 'Nº de serie',
                displayName: 'REFERENCIA',
                width: '*',
                minWidth: 170
            },
            {
                field: 'Descripción de producto',
                displayName: 'DESCRIPCIÓN',
                width: '***',
            }
        ],
        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;
            $scope.mySelectedRows = $scope.gridApi.selection.getSelectedRows();
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.molduraSelect = row.entity;
            });
        }
    }

    //calculation
    $scope.calculate = function () {

        /* $scope.precioMoldura = (($scope.molduraSelect.Precio * (($scope.widthSize / 100) * ($scope.heightSize / 100))) * 2) + ($scope.molduraSelect.Perdidas / 100); */
        $scope.precioMoldura = (((($scope.widthSize / 100) + ($scope.heightSize / 100)) * 2) + ($scope.molduraSelect.Perdidas / 100)) * $scope.molduraSelect.Precio;
        
        
        if ($scope.complementoSelect != undefined && $scope.complementoSelect.length !== 0 ) {
            $scope.precioComplemento = $scope.complementoSelect.price * (($scope.widthSize / 100) * ($scope.heightSize / 100));
            
            if ($scope.precioComplemento < $scope.complementoSelect.min) {
                $scope.precioComplemento = $scope.complementoSelect.min
            }
        } else {
            $scope.precioComplemento = 0;
        }
        
        if ($scope.acabadoSelect != undefined && $scope.acabadoSelect.length !== 0) {
            $scope.precioAcabado = $scope.acabadoSelect.price * (($scope.widthSize / 100) * ($scope.heightSize / 100));
            
            if ($scope.precioAcabado < $scope.acabadoSelect.min) {
                $scope.precioAcabado = $scope.acabadoSelect.min
            }
        } else {
            $scope.precioAcabado = 0;
        }
        
        $scope.precioProveedor = $scope.precioMoldura + $scope.precioComplemento + $scope.precioAcabado;
        $scope.pvp = (($scope.precioMoldura * 3.15) + $scope.precioComplemento + $scope.precioAcabado);
        
        console.log('moldura ' + ($scope.precioMoldura * 3.15) + 'moldura base ' + $scope.precioMoldura);
        console.log('complemento ' + $scope.precioComplemento);
        console.log('acabado ' + $scope.precioAcabado);
        
    }

    $scope.cleanData = function () {
        $scope.precioMoldura = 0;
        $scope.precioComplemento = 0;
        $scope.precioAcabado = 0;

        $scope.precioProveedor = 0;
        $scope.pvp = 0;

        $scope.widthSize = '';
        $scope.heightSize = '';

        $('.select-box select').val('');
        $scope.gridApi.selection.unSelectRow($scope.molduraSelect);
    }

}