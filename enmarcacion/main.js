var app = angular.module('app', ['ngRoute', 'ngAnimate', 'ui.grid', 'ui.grid.edit', 'ui.grid.selection', 'firebase', 'ngSanitize']);

//firebase service
const config = {
    apiKey: "",
    authDomain: "",
    databaseURL: "",
    projectId: "",
    storageBucket: "",
    messagingSenderId: ""
};

firebase.initializeApp(config);

app.factory("Auth", ["$firebaseAuth", function ($firebaseAuth) {
    return $firebaseAuth();
}]);

app.run(["$rootScope", "$location", function ($rootScope, $location) {
    $rootScope.$on("$routeChangeError", function (event, next, previous, error) {
        if ($error === "AUTH_REQUIRED") {
            $location.path("/crm");
        }
    });
}]);

//directives
app.directive('onEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown",
            function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.onEnter);
                    });
                    event.preventDefault();
                }
            });
    };
});

app.directive('onEsc', function () {
    return function (scope, element, attrs) {
        element.bind("keydown",
            function (event) {
                if (event.which === 27) {
                    scope.$apply(function () {
                        scope.$eval(attrs.onEsc);
                    });
                    event.preventDefault();
                }
            });
    };
});

//locations
app.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
        .when('/crm', {
            templateUrl: './build/views/crm/crm.html',
            controller: 'appController',
        })
        .when('/calc', {
            templateUrl: './build/views/calc/calc.html',
            controller: 'appController',
        })
        .when('/upload', {
            templateUrl: './build/views/upload/upload.html',
            controller: 'appController',
        })
        .otherwise({
            redirectTo: '/crm',
        });

}]);

//build
app.controller('appController', ['$scope', '$http', '$rootScope', '$location', '$window', '$timeout', '$interval', '$firebaseAuth', '$firebaseStorage',
    function ($scope, $http, $rootScope, $location, $window, $timeout, $interval, $firebaseAuth, $firebaseStorage) {

        //nav
        app.nav($scope, $rootScope, $location, $window, $timeout, $interval, $firebaseAuth, $firebaseStorage);

        //views
        app.crm($scope, $http, $location, $window, $timeout, $interval);
        app.calc($scope, $http, $location, $window, $timeout, $interval);
        app.upload($scope, $location, $window, $timeout, $interval);

        //firebase
        app.firebase($scope, $location, $window, $timeout, $interval, $firebaseAuth, $firebaseStorage);

    }
]);